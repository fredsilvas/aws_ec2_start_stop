# Created: André Frederico Lucas da Silva
# Gitlab: gitlab.com/fredsilvas

import json
import subprocess
import datetime
import os
import logging


logging.basicConfig(filename='/var/log/ec2_start_stop.log', filemode='w', format='%(levelname)s - %(name)s - %(asctime)s: %(message)s', datefmt='%d-%b-%y %H:%M:%S',level=logging.INFO)
logging.info('')
logging.info('#########################################################')
logging.info('# Execução do Script Iniciada')
logging.info('')

### Variaveis
dia_semana = ['Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom']
instancia_id = ''
instancia_status = ''
instancia_nome = ''
horario_especial = ''
dia_start = ''
hora_start = ''
dia_stop = ''
hora_stop = ''
checa_execucao = ''



### Pegando horario atual
now = datetime.datetime.now()
time = now.strftime("%H:%M")



### Function Stop Instances
def stop_instances():
    r = subprocess.run(["aws", "ec2", "stop-instances", "--instance-ids", instancia_id], capture_output=True)
    if r.returncode == 0:
        print ("Instância {0}, Id: {1}, desligada com sucesso!".format(instancia_nome,instancia_id))
        print ('')
        logging.info("Instância {0}, Id: {1}, desligada com sucesso!".format(instancia_nome,instancia_id))
        logging.info('')
       


### Function Start Instances
def start_instances():
    r = subprocess.run(["aws", "ec2", "start-instances", "--instance-ids", instancia_id], capture_output=True)
    if r.returncode == 0:
        print ("Instância {0}, Id: {1}, ligada com sucesso!".format(instancia_nome,instancia_id))
        print ('')
        logging.info ("Instância {0}, Id: {1}, ligada com sucesso!".format(instancia_nome,instancia_id))
        logging.info('')




### Obter lista de instancias
def get_instances_json():
    print("Buscando Lista de Instâncias com Horário Especial...\n")
    logging.info('Buscando Lista de Instâncias com Horário Especial...')

    f = open("cli.txt", "w")
    r = subprocess.run(["aws", "ec2", "describe-instances", "--filters", "Name=tag:HorarioEspecial,Values=true", "--output", "json"], stdout=f)

    if r.returncode != 0:
        print ('\nErro ao recuperar instâncias. Não é Possível Continuar!')
        print ('Veja acima informações sobre o erro.')
        print ('Código do erro: {0}'.format(r.returncode))
        print ('')
        logging.info('Erro ao recuperar instâncias. Não é Possível Continuar!')
        logging.info('Código do erro: {0}'.format(r.returncode))
        exit()
    else:
        logging.info('Lista de Instâncias carregadas com sucesso!')



### Bloco Principal
get_instances_json()

with open('cli.txt') as json_file:
    data = json.load(json_file)

    for reserva in data['Reservations']:
        
        for instancia in reserva['Instances']:
            #global instancia_id
            instancia_id = instancia['InstanceId']
            instancia_status = instancia['State']['Name']

            for tag in instancia['Tags']:
        
                if tag['Key'] == 'Name':
                    instancia_nome = tag['Value']
                    continue

                if tag['Key'] == 'HorarioEspecial':                    
                    horario_especial = tag['Value']
                    continue
                
                if tag['Key'] == 'DiaStart':
                    dia_start = tag['Value']
                    dia_start = dia_start.split(",")
                    continue

                if tag['Key'] == 'HoraStart':
                    hora_start = tag['Value']
                    continue

                if tag['Key'] == 'DiaStop':
                    dia_stop = tag['Value']
                    dia_stop = dia_stop.split(",")
                    continue

                if tag['Key'] == 'HoraStop':
                    hora_stop = tag['Value']
                    continue

            #print('{0}: {1}'.format('Nome',instancia_nome))
            #print('{0}: {1}'.format('Id',instancia_id))
            #print('{0}: {1}'.format('Status',instancia_status))
            #print('{0}: {1}'.format('Horario Especial',horario_especial))
            #print('{0}: {1}'.format('Dia de Start',dia_start))
            #print('{0}: {1}'.format('Hora de Start',hora_start))
            #print('{0}: {1}'.format('Dia de Stop',dia_stop))
            #print('{0}: {1}\n'.format('Hora de Stop',hora_stop))

            if dia_semana[datetime.datetime.now().weekday()] in dia_start:
                if time in hora_start:
                    if instancia_status == 'stopped' and horario_especial == 'true':
                        logging.info("Instância {0}, Id: {1}, será ligada.".format(instancia_nome,instancia_id))
                        checa_execucao = '1'
                        start_instances()
            #print ('Instância não cumpre as regras para Start\n')

            if dia_semana[datetime.datetime.now().weekday()] in dia_stop:
                if time in hora_stop:
                    if instancia_status == 'running' and horario_especial == 'true':
                        logging.info("Instância {0}, Id: {1}, será desligada.".format(instancia_nome,instancia_id))
                        checa_execucao = '1'
                        stop_instances()
            #print ('Instância não cumpre as regras para Stop\n')


            instancia_id = ''
            instancia_status = ''
            instancia_nome = ''
            horario_especial = ''
            dia_start = ''
            hora_start = ''
            dia_stop = ''
            hora_stop = ''

if checa_execucao != '1':
    logging.info("Nenhuma Instância necessitava ser ligada ou desligada.")    


logging.info("Limpando informações coletadas...")
#print("Limpando informações coletadas...")
os.remove("cli.txt")

logging.info('')
logging.info('# Execução do Script Finalizada')
logging.info('#########################################################')
logging.info('')