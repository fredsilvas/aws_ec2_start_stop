# Script de Start/Stop AWS EC2 Instance

Este Sript realiza o Start/Stop de Instâncias EC2, baseado em TAGs.
Normalmente este recurso é utilizado com função Lambda, porém caso não queira, ou não possa usar, o script realiza este procedimento.


## Preparação

Antes da Instalação, prepare o ambiente instalando o Python 3.8 e o AWS CLI


## IAM

Para a execução deste script, recomenda-se a criação de um usuário no AWS IAM apenas com permissão de Iniciar e Desligar as instâncias EC2.


## TAGs

Para o funcionamento deste script, é necessário que a(s) instância(s) possuam as seguintes TAGs:


|   NOME DA TAG   	|   TIPO  	|                                                                                                                                                            DESCRIÇÃO                                                                                                                                                            	|
|:---------------:	|:-------:	|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:	|
| HorarioEspecial 	| boolean 	|                                                                                                                    Informação se a instância será ligada/desligada em horário especial. Valores: true / false                                                                                                                   	|
|     DiaStart    	|  string 	|              Informação dos dias da semana que a instância deverá ser ligada. Colocar os dias da semana separados por vírgula. Valores pré definidos.  Ex: Seg,Ter,Qua,Qui,Sex,Sab,Dom (A máquina será ligada todos os dias da semana) Ex: Seg,Qua,Sex (A máquina será ligada segunda, quarta-feira e sexta-feira)              	|
|    HoraStart    	|  string 	|      Informação do horário em que a instância deverá ser ligada. Colocar o horário fechado no formato hh:mm. A execução do script acontecerá a cada 10 minutos, então só serão considerados os minutos 00, 10, 20, 30, 40 e 50 no campo “minutos”  Ex: 08:00 (A máquina será ligada às 08 horas da manhã) Ex:14:30 (A máquina será ligada às 14:30 da tarde)     	|
|     DiaStop     	|  string 	|          Informação dos dias da semana que a instância deverá ser desligada. Colocar os dias da semana separados por vírgula. Valores pré definidos.  Ex: Seg,Ter,Qua,Qui,Sex,Sab,Dom (A máquina será desligada todos os dias da semana) Ex: Seg,Qua,Sex (A máquina será desligada segunda, quarta-feira e sexta-feira)         	|
|     HoraStop    	|  string 	| Informação do horário em que a instância deverá ser desligada. Colocar o horário fechado no formato hh:mm. A execução do script acontecerá a cada 10 minutos, então só serão considerados os minutos 00, 10, 20, 30, 40 e 50 no campo “minutos”  Ex: 08:00 (A máquina será desligada às 08 horas da manhã) Ex:18:20 (A máquina será desligada às 18:20 da tarde) 	|


## CRON
Adicionar o script no cron para execução conforme desejado.
Ex: */10 * * * * <user> <executavel python 3.8> <.../aws_ec2_start_stop/script.py>


### License
[MIT](https://choosealicense.com/licenses/mit/)